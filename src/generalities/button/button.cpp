#include "button.h"
const int maxCuantityButons = 100;
button::button(short buton, Vector2 pos, Texture2D texture, short cuantity)
{
	_frameHeight = static_cast<float>(texture.height / 2); //cantidad de frames.
	_frameWidth = static_cast<float>(texture.width / cuantity);
	_state = 0;
	_inside = { _frameWidth * buton,_frameHeight * _state,_frameWidth ,_frameHeight };
	_rec = { pos.x - _frameWidth / 2,pos.y - _frameHeight / 2,_frameWidth,_frameHeight };
	_pos = pos;
	_text = " ";
}

button::button(Vector2 pos, std::string text,Texture2D tx)
{
	_frameHeight = static_cast<float>(tx.height)/2; //cantidad de frames.
	_inside = { 0,0,static_cast<float>(tx.width) ,_frameHeight };
	_rec = { pos.x - static_cast<float>(tx.width)/2,pos.y - _frameHeight / 2,static_cast<float>(tx.width),_frameHeight };
	_state = 0;
	_pos = pos;
	_text = text;
}
button::button(Vector2 pos, Texture2D tx)
{
	_frameHeight = static_cast<float>(tx.height) / 2; //cantidad de frames.
	_inside = { 0,0,static_cast<float>(tx.width) ,_frameHeight };
	_rec = { pos.x - static_cast<float>(tx.width) / 2,pos.y - _frameHeight / 2,static_cast<float>(tx.width),_frameHeight };
	_state = 0;
	_pos = pos;
	_text = " ";
}
button::button(Vector2 pos, Texture2D tx,bool pivot, std::string text)
{
	_frameHeight = static_cast<float>(tx.height) / 2; //cantidad de frames.
	_inside = { 0,0,static_cast<float>(tx.width) ,_frameHeight };
	_rec = { pos.x,pos.y,static_cast<float>(tx.width),_frameHeight };
	_state = 0;
	_pos = { pos.x + tx.width / 2,pos.y + _rec.height/2 };
	_text = text;
}

bool button::actualizar(Sound sd)
{
	Vector2 mousePoint = GetMousePosition();
	if (CheckCollisionPointRec(mousePoint, _rec))
	{
		if (_state==0)
		{
			PlaySoundMulti(sd);
		}
		_state = 1;
		_inside.y = _state * _frameHeight;
		if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
		{
			return true;
		}
		return false;
	}
	else
	{
		_state = 0;
		_inside.y = _state * _frameHeight;
		return false;

	}
	
}
bool button::actualizar()
{
	Vector2 mousePoint = GetMousePosition();
	if (CheckCollisionPointRec(mousePoint, _rec))
	{
		if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
		{
			if (_state == 1)
			{
				_state = 0;

			}
			else
				_state = 1;
			_inside.y = _state * _frameHeight;
			return true;
		}
		return false;
	}
	else
	{
		
		_inside.y = _state * _frameHeight;
		return false;

	}

}

void button::drawButon(Texture2D tx)
{
	DrawTexturePro(tx, _inside, _rec, { 0,0 }, 0, WHITE);
	int letterSize = tx.height/3;
	Color A =GRAY;
	if (_state == 1)
		A = ORANGE;
	DrawText(&_text[0], static_cast<int>(_pos.x) - MeasureText(&_text[0], letterSize) / 2, static_cast<int>(_pos.y) - letterSize /2, letterSize, A);
	
#ifdef _DEBUG

	DrawRectangleLines(static_cast<int>(_rec.x), static_cast<int>(_rec.y), static_cast<int>(_rec.width), static_cast<int>(_rec.height), RED);
	DrawRectangleLines(static_cast<int>(_rec.x), static_cast<int>(_rec.y), static_cast<int>(_inside.width), static_cast<int>(_inside.height), GREEN);
	DrawRectangleLines(static_cast<int>(0), static_cast<int>(0), static_cast<int>(tx.width), static_cast<int>(tx.height), GREEN);

#endif // DEBUG

}

button::~button()
{ }

buttonUpDown::buttonUpDown(Rectangle rec, Texture2D tx)
{
	_rec = rec;
	tx.width = static_cast<int>(_rec.width);
	tx.height = static_cast<int>(_rec.height);
	Up = new button({ _rec.x+_rec.width /2,_rec.y },"UP", tx);
	Down = new button({ _rec.x+_rec.width/2,_rec.y + _rec.height },"DOWN", tx);
}

bool buttonUpDown::actualizar(int& value,int min,int max)
{
	if (Up->actualizar(buttons::menu::sd))
	{
		if (value < max)
		{
			value++;
			return true;
		}
		else
			return false;
	}
		
	if (Down->actualizar(buttons::menu::sd))
	{
		if (value > min)
		{
			value--;
			return true;
		}
		else
			return false;
	}
	return false;
}
bool buttonUpDown::actualizarF(float& value, float min, float max)
{
	if (Up->actualizar(buttons::menu::sd))
	{
		if (value < max)
		{
			value+=0.1f;
			return true;
		}
		else
			return false;
	}

	if (Down->actualizar(buttons::menu::sd))
	{
		if (value > min)
		{
			value-=0.1f;
			return true;
		}
		else
			return false;
	}
	return false;
}
void buttonUpDown::drawButon(Texture2D tx, int value, std::string txt)
{
	tx.width = static_cast<int>(_rec.width);
	tx.height = static_cast<int>(_rec.height);
	DrawRectanglePro(_rec, { 0,0 },0, WHITE);
	Up->drawButon(tx);
	Down->drawButon(tx);
	if (value > 9) 
	{
		DrawText(txt.c_str(), static_cast<int>(_rec.x + _rec.width / 2 - MeasureText("01", control::letters::medium) / 2), static_cast<int>(_rec.y + _rec.height / 2 - control::letters::medium / 2) - control::letters::medium, control::letters::medium, BLACK);
		DrawText(FormatText(" 0%i", value), static_cast<int>(_rec.x + _rec.width / 2 - MeasureText("01", control::letters::medium) / 2), static_cast<int>(_rec.y + _rec.height / 2 - control::letters::medium / 2), control::letters::medium, BLACK);
	}
	else
	{
		DrawText(txt.c_str(), static_cast<int>(_rec.x + _rec.width / 2 - MeasureText("01", control::letters::medium) / 2), static_cast<int>(_rec.y + _rec.height / 2 - control::letters::medium / 2) - control::letters::medium, control::letters::medium, BLACK);
		DrawText(FormatText("%i", value), static_cast<int>(_rec.x + _rec.width / 2 - MeasureText("1", control::letters::medium) / 2), static_cast<int>(_rec.y + _rec.height / 2 - control::letters::medium / 2), control::letters::medium, BLACK);
	}
}
void buttonUpDown::drawButonF(Texture2D tx, float value, std::string txt)
{
	tx.width = static_cast<int>(_rec.width);
	tx.height = static_cast<int>(_rec.height);
	DrawRectanglePro(_rec, { 0,0 }, 0, WHITE);
	Up->drawButon(tx);
	Down->drawButon(tx);
	if (value > 9)
	{
		DrawText(txt.c_str(), static_cast<int>(_rec.x + _rec.width / 2 - MeasureText("01", control::letters::medium) / 2), static_cast<int>(_rec.y + _rec.height / 2 - control::letters::medium / 2) - control::letters::medium, control::letters::medium, BLACK);
		DrawText(FormatText(" 0%f", value), static_cast<int>(_rec.x + _rec.width / 2 - MeasureText("01", control::letters::medium) / 2), static_cast<int>(_rec.y + _rec.height / 2 - control::letters::medium / 2), control::letters::medium, BLACK);
	}
	else
	{
		DrawText(txt.c_str(), static_cast<int>(_rec.x + _rec.width / 2 - MeasureText("01", control::letters::medium) / 2), static_cast<int>(_rec.y + _rec.height / 2 - control::letters::medium / 2) - control::letters::medium, control::letters::medium, BLACK);
		DrawText(FormatText("%f", value), static_cast<int>(_rec.x + _rec.width / 2 - MeasureText("1", control::letters::medium) / 2), static_cast<int>(_rec.y + _rec.height / 2 - control::letters::medium / 2), control::letters::medium, BLACK);
	}
}

buttonUpDown::~buttonUpDown()
{
	delete Up;
	delete Down;
}
