#ifndef BUTON
#define BUTON
#include "raylib.h"
#include "generalities/generalities.h"
#include "generalities/button/buttonAssets.h"
#include <string>

extern const int maxCuantityButons;

enum class buttonsMenu
{
	play,
	help,
	credits,
	close,
	cuantity
};

class button
{
public:
	button(short buton, Vector2 pos, Texture2D texture, short cuantity);
	button(Vector2 pos, std::string text,Texture2D tx);
	button(Vector2 pos, Texture2D tx);
	button(Vector2 pos, Texture2D tx, bool pivot, std::string text);
	bool actualizar(Sound sd);
	bool actualizar();
	void drawButon(Texture2D tx);
	~button();

private:
	Vector2			_pos; //pivot en centro.
	Rectangle		_rec; //rect en pantalla.
	float   _frameHeight; //alto del frame.
	float    _frameWidth; //ancho del frame.
	Rectangle	 _inside; //rec interno del texture.
	short		  _state; //estado.
	std::string    _text; //que dice.
};
class buttonUpDown
{
public:
	buttonUpDown(Rectangle rec, Texture2D tx);
	bool actualizar(int& value, int min, int max);
	bool actualizarF(float& value, float min, float max);
	void drawButon(Texture2D tx, int value, std::string txt);
	void drawButonF(Texture2D tx, float value, std::string txt);
	~buttonUpDown();

private:
	button* Up;
	button* Down;
	Rectangle _rec; //rect en pantalla.
};
#endif
