#ifndef BUTTONASSETS
#define BUTTONASSETS
#include "raylib.h"
namespace buttons
{
	namespace menu
	{
		extern Texture2D tx;
		extern Sound sd;
		void chargeButonAssets(Texture2D tex, Sound snd);
		void dischargButonAssets();
	}
	namespace sounds
	{
		extern Texture2D tx;
		extern Texture2D tx1;
	}
	namespace levels
	{
		extern Texture2D tx;
	}
}

#endif // !BUTTONASETTS
