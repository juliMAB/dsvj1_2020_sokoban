#ifndef GENERALITIES
#define GENERALITIES
#include "raylib.h"
#include <string>
namespace control
{
	enum class Scene
	{
		Menu,
		Help,
		Levels,
		Credits,
		Game,
		Creator,
		NONE
	};
	namespace sounds
	{
		extern Music musicGeneral;
		extern float volumeGeneral;
		extern float volumeFx;
		extern float volumeMusic;
	}
	namespace Logic
	{
		extern bool gameOver;
		extern Scene currentScene;
		extern Scene newCurrentScene;
		const int levelsMax=17;
		const int maxGrill = 100;
		extern int unlookLevel;
		extern int currentLevel;
		extern int levelSelecct;
	}
	namespace letters
	{
		extern Font font;
		extern int small;
		extern int medium;
		extern int big;
	}
	namespace screen
	{
		extern int height;
		extern int width;
		extern int resolutionSelected;
		const int currentresolution[5][2] =
		{
			{240,320},
			{300,400},
			{480,640},
			{600,800},
			{800,1066},
		};
		extern bool changeResolution;
	}
	namespace leveles
	{
		const std::string L[18]= {
		"level1.txt","level2.txt","level3.txt",
		"level4.txt","level5.txt","level6.txt","level7.txt","level8.txt",
		"level9.txt","level10.txt","level11.txt","level12.txt","level13.txt",
		"level14.txt","level15.txt","levelP1.txt","levelP2.txt","levelP3.txt"
		};
		extern int stars[18];
		/*const int maxSteps[18][3]
		{
			{5,6,7},{49,60,70},{23,30,40},{10,20,30},
			{10,20,30},{10,20,30},{10,20,30},{10,20,30},
			{10,20,30},{10,20,30},{10,20,30},{10,20,30},
			{10,20,30},{10,20,30},{10,20,30},{10,20,30},
			{10,20,30},{10,20,30}
		};*/
	}
	namespace tx
	{
		extern Texture2D star;
		extern Texture2D matrix;
		extern Texture2D fondo;
	}
}
char IntToChar(int a);
//Float to int to char.
char fic(float a);
int ci(char a);
const int maxreaddata = 10005;
char icB(int a);
int ciB(char a);
#endif // !GENERAlITIES
