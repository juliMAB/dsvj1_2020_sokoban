#include "maps.h"
#include <string>
maps::maps()
{
	defaultChargeData();
	//----------------Crear marco---------------
	createUpdateFramework();
	//--------------------------------------------
	chargeMap();
	loadTexures();
}
maps::maps(bool game, const std::string file)
{
	//----------------Crear marco---------------
	createUpdateFrameworkGame();
	//--------------------------------------------
	chargeMap(file);
	loadTexures();
	updateSizeGrid();
	setpjRec();
	setboxesRec();
	
}
void maps::setboxesRec()
{
	for (int i = 0; i < cuantitis::maxBoxes; i++)
	{
		boxesRec[i] = { _framework.x + posCajas[i].x * sizeGrid,_framework.y + posCajas[i].y * sizeGrid,sizeGrid,sizeGrid };
	}
}
void maps::setpjRec()
{
	pjRec = { _framework.x+posMono.x * sizeGrid,_framework.y + posMono.y * sizeGrid,sizeGrid,sizeGrid };
	tx::pj.width = static_cast<int>(sizeGrid * 6);
	tx::pj.height = static_cast <int>(sizeGrid * 3);
	pjAction = moving::stop;
}
void maps::loadTexures()
{
	tx::box = LoadTexture("res/assets/box.png");
	tx::floor = LoadTexture("res/assets/piso.png");
	tx::pj = LoadTexture("res/assets/monoX6.png");
	tx::dely = LoadTexture("res/assets/deliv.png");
	tx::wall = LoadTexture("res/assets/wall.png");
}
void maps::defaultChargeData()
{
	//despues de muchos errores decubri que no se puede acceder a un espacio de texto vacio o nullo, tiene que estar con algo.
	//de no ser asi el programa se rompe.
	char* data = LoadFileText("clearfile.txt");
	for (int i = 0; i < maxreaddata; i++)
	{
		data[i] = '0';
	}
	SaveFileText("clearfile.txt", data);
}
void maps::loadColumsrowsBoxes(char* data)
{
	colums = ciB(data[0]) * 10 + ciB(data[1]);
	rows = ciB(data[2]) * 10 + ciB(data[3]);
	_cuantitiBoxes = ciB(data[4]) * 10 + ciB(data[5]);
}
void maps::loadPosMono(char* data)
{
	posMono = { static_cast<float>(((ciB(data[6])) * 10) + ciB(data[7])),static_cast<float>(((ciB(data[8])) * 10) + ciB(data[9])) };
	pjRec = { posMono.x*sizeGrid,posMono.y * sizeGrid,sizeGrid,sizeGrid };
}
void maps::loadPosBoxes(char* data)
{
	//el maximo de cajas que se pueden crear son 20
	//dando un total de 20*4 datos= 80 datos de cajas maximo.
	//como utilizo los espacios hasta 9, desde el 10 al 90 son cajaspos.
	int contador = 10;
	for (int i = 0; i < _cuantitiBoxes; i++)
	{
		posCajas[i] = { static_cast<float>((ciB(data[contador]) * 10) + ciB(data[contador + 1])),static_cast<float>((ciB(data[contador + 2]) * 10) + ciB(data[contador + 3])) };
		contador += 4;
	}
}
void maps::loadPosDelivery(char* data)
{
	//100 a 180 son deliverypoints.
	int contador = 100;
	for (int i = 0; i < _cuantitiBoxes; i++)
	{
		posDeliv[i] = { static_cast<float>(ciB(data[contador]) * 10 + ciB(data[contador + 1])),static_cast<float>(ciB(data[contador + 2]) * 10 + ciB(data[contador + 3])) };
		contador += 4;
	}
}
void maps::loadGround(char* data)
{
	//en el caso de usar 99 x 99 matrix, el total de cosas guardadas es 9801(de 200 a 10001) ints, es mas facil guardar solo
	//las pociciones de los objetos que todos.
	int contador = 200;
	for (int i = 0; i < rows; i++)
	{
		for (int w = 0; w < colums; w++)
		{
			_floor[i][w] = ground::NONE;
			_fisic[i][w] = objFisics::NONE;
			if ((i == ciB(data[contador]) * 10 + ciB(data[contador + 1])) && w == (ciB(data[contador + 2]) * 10 + ciB(data[contador + 3])))
			{
				
				_floor[i][w] = ground::tile;
				
				
				contador += 4;
			}
			if (i == posMono.x && w == posMono.y)
				_fisic[i][w] = objFisics::pj;
			for (int z = 0; z < _cuantitiBoxes; z++)
			{
				if (i == posDeliv[z].x && w == posDeliv[z].y)
					_floor[i][w] = ground::deliverypoint;

			}
			for (int z = 0; z < _cuantitiBoxes; z++)
			{
				if (i == posCajas[z].x && w == posCajas[z].y)
					_fisic[i][w] = objFisics::box;
			}
		}
	}
	//------------------------------------------
}
void maps::saveColumsrowsBoxes(char* level)
{
	//---------------------- Columnas, filas y cantidad cajas------------------------
	level[0] = icB(static_cast<int>(colums / 10));
	level[1] = icB(static_cast<int>(colums - ciB(level[0]) * 10));
	level[2] = icB(static_cast<int>(rows / 10));
	level[3] = icB(static_cast<int>(rows - ciB(level[2]) * 10));
	level[4] = icB(static_cast<int>(_cuantitiBoxes / 10));
	level[5] = icB(static_cast<int>(_cuantitiBoxes - ciB(level[4]) * 10));
	//-------------------------------------------------------------------------------
}
void maps::savePlayer(char* level)
{
	//-----------------------------------pos pj-------------------------------------
	for (int i = 0; i < colums; i++)
	{
		for (int w = 0; w < rows; w++)
		{
			if (_fisic[i][w] == objFisics::pj)
			{
				posMono.x = static_cast<float>(i);
				posMono.y = static_cast<float>(w);
			}
		}
	}
	level[6] = icB(static_cast<int>(posMono.x / 10));
	level[7] = icB(static_cast<int>(posMono.x) - ciB(level[6]));
	level[8] = icB(static_cast<int>(posMono.y / 10));
	level[9] = icB(static_cast<int>(posMono.y) - ciB(level[8]));
	//-------------------------------------------------------------------------------
}
void maps::saveDeliveryPoints(char* level)
{
	//100 a 180 son deliverypoints.
	int contador = 100;
	int a = 0;
	for (int i = 0; i < colums; i++)
	{
		for (int w = 0; w < rows; w++)
		{
			if (_floor[i][w] == ground::deliverypoint)
			{
				posDeliv[a] = { static_cast<float>(i),static_cast<float>(w) };
				a++;
			}
		}
	}
	for (int i = 0; i < _cuantitiBoxes; i++)
	{
		level[contador] = icB(static_cast<int>(posDeliv[i].x / 10));
		level[contador + 1] = icB(static_cast<int>(posDeliv[i].x - ciB(level[contador]) * 10));
		level[contador + 2] = icB(static_cast<int>(posDeliv[i].y / 10));
		level[contador + 3] = icB(static_cast<int>(posDeliv[i].y - ciB(level[contador + 2]) * 10));
		contador += 4;
	}
}
void maps::saveBoxes(char* level)
{
	//---------------------Guardar Cajas---------------------------------------------
	//el maximo de cajas que se pueden crear son 20
	//dando un total de 20*4 datos= 80 datos de cajas maximo.
	//como utilizo los espacios hasta 9, desde el 10 al 90 son cajaspos.
	int contador = 10;
	int save = 0;
	for (int i = 0; i < colums; i++)
	{
		for (int w = 0; w < rows; w++)
		{
			if (_fisic[i][w]==objFisics::box)
			{
				posCajas[save].x = static_cast<float>(i);
				posCajas[save].y = static_cast<float>(w);
					save++;
			}
			
		}
	}
	for (int i = 0; i < _cuantitiBoxes; i++)
	{
		level[contador] = icB(static_cast<int>(posCajas[i].x / 10));
		level[contador + 1] = icB(static_cast<int>(posCajas[i].x) - ciB(level[contador]) * 10);
		level[contador + 2] = icB(static_cast<int>(posCajas[i].y / 10));
		level[contador + 3] = icB(static_cast<int>(posCajas[i].y) - ciB(level[contador + 2]) * 10);
		contador += 4;
	}
}
void maps::SaveTiles(char* level)
{
	int contador = 200;
	for (int i = 0; i < rows; i++)
	{
		for (int w = 0; w < colums; w++)
		{
			if (_floor[i][w] == ground::tile)
			{
				level[contador] = icB(static_cast<int>(i / 10));
				level[contador + 1] = icB(i - ciB(level[contador]) * 10);
				level[contador + 2] = icB(static_cast<int>(w / 10));
				level[contador + 3] = icB(w - ciB(level[contador - 2]) * 10);
				contador += 4;
			}
		}
	}
}
void maps::saveMap(const std::string file)
{
	char* level= LoadFileText("clearfile.txt");
	saveColumsrowsBoxes(level);
	savePlayer(level);
	saveBoxes(level);
	saveDeliveryPoints(level);
	SaveTiles(level);
	//last step.
	SaveFileText(file.c_str(),level);
}
void maps::chargeMap()
{
	char* data = LoadFileText(control::leveles::L[control::Logic::levelSelecct].c_str());
	loadColumsrowsBoxes(data);
	loadPosMono(data);
	loadPosBoxes(data);
	loadPosDelivery(data);
	loadGround(data);
}
void maps::chargeMap(const std::string file)
{
	char* data = LoadFileText(file.c_str());
	loadColumsrowsBoxes(data);
	loadPosMono(data);
	loadPosBoxes(data);
	loadPosDelivery(data);
	loadGround(data);
}
void maps::createUpdateFramework()
{
	_framework = { static_cast<float>(GetScreenWidth() / 10 * 1),0,static_cast<float>(GetScreenWidth() / 10 * 8),static_cast<float>(GetScreenHeight() / 10 * 7) };
	
}
void maps::createUpdateFrameworkGame()
{
	_framework = { static_cast<float>(GetScreenWidth() / 100 * 5),static_cast<float>(GetScreenHeight() / 100 * 5),static_cast<float>(GetScreenWidth() / 10 * 9),static_cast<float>(GetScreenHeight() / 10 * 9) };
	control::tx::matrix = LoadTexture("res/assets/fabrica.png");
	control::tx::matrix.height = static_cast<int>(_framework.height);
	control::tx::matrix.width = static_cast<int>(_framework.width);
}
void maps::updateTexture()
{
	tx::box.width = static_cast<int>(sizeGrid);
	tx::box.height = static_cast<int>(sizeGrid);
	tx::dely.width = static_cast<int>(sizeGrid);
	tx::dely.height = static_cast<int>(sizeGrid);
	tx::floor.width = static_cast<int>(sizeGrid);
	tx::floor.height = static_cast<int>(sizeGrid);
	tx::pj.width = static_cast<int>(sizeGrid*6);
	tx::pj.height = static_cast < int>(sizeGrid*3);
	tx::wall.width = static_cast<int>(sizeGrid);
	tx::wall.height = static_cast<int>(sizeGrid);
}
void maps::updateSizeGrid()
{
	sizeGrid = static_cast<float>(_framework.height / rows);
	if (sizeGrid * colums > _framework.width && sizeGrid * rows > _framework.height)
	{
		if (_framework.height/rows>_framework.width/colums)
		{
			sizeGrid = static_cast<float>(_framework.width / colums);
			updateTexture();
		}
		else
		{
			sizeGrid = static_cast<float>(_framework.height / rows);
			updateTexture();
		}
	}
	if (sizeGrid * colums > _framework.width && sizeGrid * rows <= _framework.height)
	{
		sizeGrid = static_cast<float>(_framework.width / colums);
		updateTexture();
	}
	if (sizeGrid * colums <= _framework.width && sizeGrid * rows > _framework.height)
	{
		sizeGrid = static_cast<float>(_framework.height / rows);
		updateTexture();
	}
	Merror = 45 * 0.0005f / sizeGrid;
}
void maps::showInfo()
{
	int boxesInMap=0;
	int devileyPoints = 0;
	for (int i = 0; i < rows; i++)
	{
		for (int w = 0; w < colums; w++)
		{
			if (_fisic[i][w]==objFisics::box)
			{
				boxesInMap++;
			}
			if (_floor[i][w]==ground::deliverypoint)
			{
				devileyPoints++;
			}
		}
	}
	DrawText(FormatText ("BOXES IN MAP %i/%i ------ DELIVERYPOINTS IN MAP %i/%i",boxesInMap,_cuantitiBoxes,devileyPoints,_cuantitiBoxes), static_cast<int>(_framework.x), static_cast<int>(_framework.y),control::letters::medium,RED);
}
void maps::update()
{
	updateCuanitys();
	createUpdateFramework();
	updateSizeGrid();
	inputHacks();
}
void maps::updateCuanitys()
{
	_cuantitiBoxes = 0;
	for (int i = 0; i < colums; i++)
	{
		for (int w = 0; w < rows; w++)
		{
			if (_fisic[i][w]==objFisics::box)
			{
				_cuantitiBoxes++;
			}
		}
	}
}
void maps::updateOnGame()
{
	updateMatrix();
	updatePlayerBox();
	if (IsKeyPressed(KEY_R))
		reset(control::leveles::L[control::Logic::currentLevel]);
}
void maps::inputHacks()
{
	if (IsKeyPressed(KEY_C))
	{
		defaultClear();
	}
}
enableMove maps::pjEnableMoves(Vector2 obj)
{
	bool up = false;
	if (static_cast<int>(obj.y) - 1 > 0 && _floor[static_cast<int>(obj.x)][static_cast<int>(obj.y) -1] != ground::NONE)
	{
		up = 
			(((_floor[static_cast<int>(obj.x)][static_cast<int>(obj.y) - 1] == ground::tile) ||
			(_floor[static_cast<int>(obj.x)][static_cast<int>(obj.y) - 1] == ground::deliverypoint)) &&
			((_fisic[static_cast<int>(obj.x)][static_cast<int>(obj.y) - 1] == objFisics::NONE)));
		if (static_cast<int>(obj.y) - 2 > 0 && _floor[static_cast<int>(obj.x)][static_cast<int>(obj.y) - 2] != ground::NONE)
		{
			if ((_fisic[static_cast<int>(obj.x)][static_cast<int>(obj.y) - 1] == objFisics::box))
			{
				up = 
					(((_floor[static_cast<int>(obj.x)][static_cast<int>(obj.y) - 1] == ground::tile) ||
					(_floor[static_cast<int>(obj.x)][static_cast<int>(obj.y) - 1] == ground::deliverypoint)) &&
					((_floor[static_cast<int>(obj.x)][static_cast<int>(obj.y) - 2] == ground::tile) ||
					(_floor[static_cast<int>(obj.x)][static_cast<int>(obj.y) - 2] == ground::deliverypoint)) &&
					(_fisic[static_cast<int>(obj.x)][static_cast<int>(obj.y) - 2] == objFisics::NONE));
			}
		}
	}
	bool down = false;
	if (static_cast<int>(obj.y) + 1 < rows && _floor[static_cast<int>(obj.x)][static_cast<int>(obj.y) + 1] != ground::NONE)
	{
		down = 
			(((_floor[static_cast<int>(obj.x)][static_cast<int>(obj.y) + 1] == ground::tile)||
			(_floor[static_cast<int>(obj.x)][static_cast<int>(obj.y) + 1] == ground::deliverypoint)) &&
			((_fisic[static_cast<int>(obj.x)][static_cast<int>(obj.y) + 1] == objFisics::NONE)));
		if (static_cast<int>(obj.y) + 2 < rows && _floor[static_cast<int>(obj.x)][static_cast<int>(obj.y)+2] != ground::NONE)
		{
			if ((_fisic[static_cast<int>(obj.x)][static_cast<int>(obj.y) + 1] == objFisics::box))
			{
				down = 
					(((_floor[static_cast<int>(obj.x)][static_cast<int>(obj.y) + 1] == ground::tile)||
					(_floor[static_cast<int>(obj.x)][static_cast<int>(obj.y) + 1] == ground::deliverypoint)) &&
					((_floor[static_cast<int>(obj.x)][static_cast<int>(obj.y) + 2] == ground::tile) ||
					(_floor[static_cast<int>(obj.x)][static_cast<int>(obj.y) + 2] == ground::deliverypoint)) &&
					(_fisic[static_cast<int>(obj.x)][static_cast<int>(obj.y) + 2] == objFisics::NONE));
			}
			
		}
	}
	bool left = false;
	if (static_cast<int>(obj.x) - 1 > 0 && _floor[static_cast<int>(obj.x) - 1][static_cast<int>(obj.y)] != ground::NONE)
	{
		left = 
			(((_floor[static_cast<int>(obj.x)-1][static_cast<int>(obj.y)] == ground::tile) ||
			(_floor[static_cast<int>(obj.x)-1][static_cast<int>(obj.y)] == ground::deliverypoint)) &&
			((_fisic[static_cast<int>(obj.x)-1][static_cast<int>(obj.y)] == objFisics::NONE)));
		if (static_cast<int>(obj.x) - 2 > 0 && _floor[static_cast<int>(obj.x) - 2][static_cast<int>(obj.y)] != ground::NONE)
		{
			if ((_fisic[static_cast<int>(obj.x) - 1][static_cast<int>(obj.y)] == objFisics::box))
			{
				left = 
					(((_floor[static_cast<int>(obj.x)-1][static_cast<int>(obj.y)] == ground::tile) ||
						(_floor[static_cast<int>(obj.x)-1][static_cast<int>(obj.y)] == ground::deliverypoint)) &&
						((_floor[static_cast<int>(obj.x)-2][static_cast<int>(obj.y)] == ground::tile) ||
							(_floor[static_cast<int>(obj.x-2)][static_cast<int>(obj.y)] == ground::deliverypoint)) &&
						(_fisic[static_cast<int>(obj.x)-2][static_cast<int>(obj.y)] == objFisics::NONE));
			}
			
		}
	}
	bool right = false;
	if (static_cast<int>(obj.x) + 1 < rows && _floor[static_cast<int>(obj.x) + 1][static_cast<int>(obj.y)] != ground::NONE)
	{
		right = 
			(((_floor[static_cast<int>(obj.x)+1][static_cast<int>(obj.y)] == ground::tile) ||
				(_floor[static_cast<int>(obj.x)+1][static_cast<int>(obj.y)] == ground::deliverypoint)) &&
				((_fisic[static_cast<int>(obj.x)+1][static_cast<int>(obj.y)] == objFisics::NONE)));
		if (static_cast<int>(obj.x) + 2 < rows && _floor[static_cast<int>(obj.x) + 2][static_cast<int>(obj.y)] != ground::NONE)
		{
			if ((_fisic[static_cast<int>(obj.x) + 1][static_cast<int>(obj.y)] == objFisics::box))
			{
				right =
					(((_floor[static_cast<int>(obj.x)+1][static_cast<int>(obj.y)] == ground::tile) ||
						(_floor[static_cast<int>(obj.x)+1][static_cast<int>(obj.y)] == ground::deliverypoint)) &&
						((_floor[static_cast<int>(obj.x)+2][static_cast<int>(obj.y)] == ground::tile) ||
							(_floor[static_cast<int>(obj.x)+2][static_cast<int>(obj.y)] == ground::deliverypoint)) &&
						(_fisic[static_cast<int>(obj.x)+2][static_cast<int>(obj.y)] == objFisics::NONE));
			}
			
		}
	}
	return { up,down,left,right };
}
void maps::updatePlayerBox() 
{
	updatePlayer(pjEnableMoves(posMono));
	for (int i = 0; i < _cuantitiBoxes; i++)
	{
		if (CheckCollisionRecs(pjRec,boxesRec[i]))
		{
			updateBox(boxesRec[i], pjAction, posCajas[i]);
		}
	}
}
void maps::updateMatrix()
{
	for (int i = 0; i < colums; i++)
	{
		for (int w = 0; w < rows; w++)
		{
			_fisic[i][w] = objFisics::NONE;
			if (posMono.x == i && posMono.y == w)
			{
				_fisic[i][w] = objFisics::pj;
			}
			for (int j = 0; j < _cuantitiBoxes; j++)
			{
				if (posCajas[j].x==i&&posCajas[j].y==w)
				{
					_fisic[i][w] = objFisics::box;
				}
			}
		}
	}
}
void maps::updateBox(Rectangle & obj, moving action,Vector2 & pos)
{
	switch (action)
	{
	case moving::stop:
		if (((obj.y - _framework.y) / sizeGrid) != (pos.y))
		{
			if (((obj.y - _framework.y) / sizeGrid) < (pos.y)/2)
			{
				obj.y = ((pos.y - 1) * sizeGrid) + _framework.y;
				pos.y--;
			}
			if (((obj.y - _framework.y) / sizeGrid) > (pos.y)/2)
			{
				obj.y = ((pos.y + 1) * sizeGrid) + _framework.y;
				pos.y++;
			}
		}
		if (((obj.x - _framework.x) / sizeGrid) != (pos.x))
		{
			if (((obj.x - _framework.x) / sizeGrid) < (pos.x) / 2)
			{
				obj.x = ((pos.x - 1) * sizeGrid) + _framework.x;
				pos.x--;
			}
			if (((obj.x - _framework.x) / sizeGrid) > (pos.x) / 2)
			{
				obj.x = ((pos.y + 1) * sizeGrid) + _framework.x;
				pos.x++;
			}
		}
		break;
	case moving::right:
		if (((obj.x - _framework.x) / sizeGrid) != static_cast<float>(pos.x + 1))
		{
			obj.x += sizeGrid / cuantitis::stm;
		}
		if (static_cast<float>(static_cast<int>(obj.x - _framework.x) / sizeGrid) >= static_cast<float>(pos.x + 1)-0.01f)
		{
			obj.x = ((pos.x + 1) * sizeGrid) + _framework.x;
			pos.x++;
		}
		break;
	case moving::left:
		if (((obj.x - _framework.x) / sizeGrid) != static_cast<float>(pos.x - 1))
		{
			obj.x -= sizeGrid / cuantitis::stm;
		}
		if (static_cast<float>(static_cast<int>(obj.x - _framework.x) / sizeGrid) <= static_cast<float>(pos.x - 1))
		{
			obj.x = ((pos.x - 1) * sizeGrid) + _framework.x;
			pos.x--;
		}
		break;
	case moving::up:
		if (((obj.y - _framework.y) / sizeGrid) != static_cast<float>(pos.y - 1))
		{
			obj.y -= sizeGrid / cuantitis::stm;
		}
		if (static_cast<float>(static_cast<int>(obj.y - _framework.y) / sizeGrid) <= static_cast<float>(pos.y - 1))
		{
			obj.y = ((pos.y - 1) * sizeGrid) + _framework.y;
			pos.y--;
		}
		break;
	case moving::down:
		if (((obj.y - _framework.y) / sizeGrid) != static_cast<float>(pos.y + 1))
		{
			obj.y += sizeGrid / cuantitis::stm;
		}
		if (static_cast<float>(static_cast<int>(obj.y - _framework.y) / sizeGrid) >= static_cast<float>(pos.y + 1) - 0.01f)
		{
			obj.y = ((pos.y + 1) * sizeGrid) + _framework.y;
			pos.y++;
		}
		break;
	}
}
void maps::animplayer()
{
	if (pjTimer>0)
	{
		switch (pjAction)
		{
		case moving::stop:
			pjInside = { 0,0,sizeGrid,sizeGrid };
			break;
		case moving::right:
			if (pjInside.x == sizeGrid * 5)
			{
				pjInside = { sizeGrid * 0,0,sizeGrid,sizeGrid };
			}
			if (pjInside.x == sizeGrid * 4)
			{
				pjInside = { sizeGrid * 5,0,sizeGrid,sizeGrid };
			}
			if (pjInside.x == sizeGrid * 3)
			{
				pjInside = { sizeGrid * 4,0,sizeGrid,sizeGrid };
			}
			if (pjInside.x == sizeGrid * 2)
			{
				pjInside = { sizeGrid * 3,0,sizeGrid,sizeGrid };
			}
			if (pjInside.x== sizeGrid * 1)
			{
				pjInside = { sizeGrid * 2,0,sizeGrid,sizeGrid };
			}
			if (pjInside.x == 0)
			{
pjInside = { sizeGrid * 1,0,sizeGrid,sizeGrid };
			}
			break;
		case moving::left:
			if (pjInside.x == sizeGrid * 5)
			{
				pjInside = { sizeGrid * 0,sizeGrid,sizeGrid,sizeGrid };
			}
			if (pjInside.x == sizeGrid * 4)
			{
				pjInside = { sizeGrid * 5,sizeGrid,sizeGrid,sizeGrid };
			}
			if (pjInside.x == sizeGrid * 3)
			{
				pjInside = { sizeGrid * 4,sizeGrid,sizeGrid,sizeGrid };
			}
			if (pjInside.x == sizeGrid * 2)
			{
				pjInside = { sizeGrid * 3,sizeGrid,sizeGrid,sizeGrid };
			}
			if (pjInside.x == sizeGrid * 1)
			{
				pjInside = { sizeGrid * 2,sizeGrid,sizeGrid,sizeGrid };
			}
			if (pjInside.x == 0)
			{
				pjInside = { sizeGrid * 1,sizeGrid,sizeGrid,sizeGrid };
			}
			break;
		case moving::up:
			if (pjInside.x == sizeGrid * 2)
			{
				pjInside = { sizeGrid * 0,sizeGrid * 2,sizeGrid,sizeGrid };
			}
			if (pjInside.x == sizeGrid * 1)
			{
				pjInside = { sizeGrid * 2,sizeGrid * 2,sizeGrid,sizeGrid };
			}
			if (pjInside.x == 0)
			{
				pjInside = { sizeGrid * 1,sizeGrid * 2,sizeGrid,sizeGrid };
			}
			break;
		case moving::down:
			if (pjInside.x == sizeGrid * 5)
			{
				pjInside = { sizeGrid * 0,sizeGrid * 2,sizeGrid,sizeGrid };
			}
			if (pjInside.x == sizeGrid * 4)
			{
				pjInside = { sizeGrid * 5,sizeGrid * 2,sizeGrid,sizeGrid };
			}
			if (pjInside.x == sizeGrid * 0)
			{
				pjInside = { sizeGrid * 4,sizeGrid * 2,sizeGrid,sizeGrid };
			}
			break;
		}
	}
	else
	{
	pjTimer = 5;
	}
}
void maps::updatePlayer(enableMove moves)
{
	animplayer();
	movePj(moves);
	switch (pjAction)
	{
	case moving::stop:
		pjInside = { 0,0,sizeGrid,sizeGrid };
		break;
	case moving::right:
		if (((pjRec.x - _framework.x) / sizeGrid) != static_cast<float>(posMono.x + 1))
		{
			pjRec.x += sizeGrid / cuantitis::stm;

			if (((pjRec.x - _framework.x) / sizeGrid) > static_cast<float>(posMono.x + 1))
			{
				pjRec.x = ((posMono.x + 1) * sizeGrid) + _framework.x;
			}
		}
		else
		{
			pjAction = moving::stop;
			posMono.x++;
		}
		break;
	case moving::left:
		if (((pjRec.x - _framework.x) / sizeGrid) != static_cast<float>(posMono.x - 1))
		{
			pjRec.x -= sizeGrid / cuantitis::stm;

			if (((pjRec.x - _framework.x) / sizeGrid) < static_cast<float>(posMono.x - 1))
			{
				pjRec.x = ((posMono.x - 1) * sizeGrid) + _framework.x;
			}
		}
		else
		{
			pjAction = moving::stop;
			posMono.x--;
		}
		break;
	case moving::up:
		if (((pjRec.y - _framework.y) / sizeGrid) != static_cast<float>(posMono.y - 1))
		{
			pjRec.y -= sizeGrid / cuantitis::stm;

			if (((pjRec.y - _framework.y) / sizeGrid) < static_cast<float>(posMono.y - 1))
			{
				pjRec.y = ((posMono.y - 1) * sizeGrid) + _framework.y;
			}
		}
		else
		{
			pjAction = moving::stop;
			posMono.y--;
		}
		break;
	case moving::down:
		if (((pjRec.y - _framework.y) / sizeGrid)  != static_cast<float>(posMono.y + 1))
		{
			pjRec.y += sizeGrid / cuantitis::stm;
			
			if (((pjRec.y - _framework.y) / sizeGrid) > static_cast<float>(posMono.y + 1))
			{
				pjRec.y = ((posMono.y + 1) * sizeGrid) + _framework.y;
			}
		}	
		else
		{
			pjAction = moving::stop;
			posMono.y++;
		}
		break;
	}
}
void maps::movePj(enableMove moves)
{
	if (pjAction == moving::stop)
	{
		if (moves.right)
		{
			if (IsKeyPressed(KEY_D) || IsKeyPressed(KEY_RIGHT))
			{
				pjAction = moving::right;
				_fisic[static_cast<int>(posMono.x)][static_cast<int>(posMono.y)] = objFisics::NONE;
			}
		}
		if (moves.left)
		{
			if (IsKeyPressed(KEY_A) || IsKeyPressed(KEY_LEFT))
			{
				pjAction = moving::left;
				_fisic[static_cast<int>(posMono.x)][static_cast<int>(posMono.y)] = objFisics::NONE;
			}
		}
		if (moves.up)
		{
			if (IsKeyPressed(KEY_W) || IsKeyPressed(KEY_UP))
			{
				pjAction = moving::up;
				_fisic[static_cast<int>(posMono.x)][static_cast<int>(posMono.y)] = objFisics::NONE;
			}
		}
		if (moves.down)
		{
			if (IsKeyPressed(KEY_S) || IsKeyPressed(KEY_DOWN))
			{
				pjAction = moving::down;
				_fisic[static_cast<int>(posMono.x)][static_cast<int>(posMono.y)] = objFisics::NONE;
			}
		}
	}
	
}
bool maps::win()
{
	int a = 0;
	for (int i = 0; i < colums; i++)
	{
		for (int w = 0; w < rows; w++)
		{
			if (_floor[i][w] == ground::deliverypoint)
			{
				posDeliv[a] = { static_cast<float>(i),static_cast<float>(w) };
				a++;
			}
		}
	}
	int count=0;
	for (int i = 0; i < _cuantitiBoxes; i++)
	{
		for (int w = 0; w < _cuantitiBoxes; w++)
		{
			if (posCajas[w].x == posDeliv[i].x && posCajas[w].y == posDeliv[i].y)
			{
				count++;
			}
		}
		
	}
	if (count == _cuantitiBoxes)
		return true;
	return false;
}
bool maps::lookArrown(int x, int y)
{
	if (x+1<colums)
	{
		if (_floor[x+1][y] != ground::NONE)
			return true;
	}
	if (x-1>=0)
	{
		if (_floor[x - 1][y] != ground::NONE)
			return true;
	}
	if (y+1<rows)
	{
		if (_floor[x][y+1] != ground::NONE)
			return true;
	}
	if (y-1>=0)
	{
		if (_floor[x][y-1] != ground::NONE)
			return true;
	}
	return false;
}
void maps::reset(std::string file)
{
	//----------------Crear marco---------------
	createUpdateFrameworkGame();
	//--------------------------------------------
	chargeMap(file);
	loadTexures();
	updateSizeGrid();
	setpjRec();
	setboxesRec();
}
void maps::drawGame()
{
	DrawTexturePro(control::tx::matrix, { 0,0,static_cast<float>(control::tx::matrix.width),static_cast<float>(control::tx::matrix.height) }, _framework, {0,0},0,WHITE);
	Rectangle ABC;
	for (int i = 0; i < colums; i++)
	{
		for (int w = 0; w < rows; w++)
		{
			ABC = { (i * sizeGrid + _framework.x), (w * sizeGrid + _framework.y), (sizeGrid), (sizeGrid) };
			DrawRectangleLines(static_cast<int>(i * sizeGrid + _framework.x), static_cast<int>(w * sizeGrid + _framework.y), static_cast<int>(sizeGrid), static_cast<int>(sizeGrid), RED);
			if (_floor[i][w] == ground::tile)
			{
				DrawTexturePro(tx::floor, { 0,0,static_cast<float>(tx::floor.width),static_cast<float>(tx::floor.height) }, ABC, { 0,0 }, 0, WHITE);
			}
			if (_floor[i][w] == ground::deliverypoint)
			{
				DrawTexturePro(tx::dely, { 0,0,static_cast<float>(tx::floor.width),static_cast<float>(tx::floor.height) }, ABC, { 0,0 }, 0, WHITE);
			}
			if (_floor[i][w] == ground::NONE)
			{
				if (lookArrown(i,w))
				{
					DrawTexturePro(tx::wall, { 0,0,static_cast<float>(tx::wall.width),static_cast<float>(tx::wall.height) }, ABC, { 0,0 }, 0, WHITE);

				}
			}
		}
	}
	
	DrawTexturePro(tx::pj, pjInside, pjRec, { 0,0 }, 0, WHITE);
	for (int i = 0; i < _cuantitiBoxes; i++)
	{
		Color x = WHITE;
		if (_floor[static_cast<int>(posCajas[i].x)][static_cast<int>(posCajas[i].y)] == ground::deliverypoint)
			x = GREEN;
		DrawTexturePro(tx::box, { 0,0,static_cast<float>(tx::box.width),static_cast<float>(tx::box.height) }, boxesRec[i], {0,0},0,x);
	}
	DrawRectangleLinesEx(_framework, 1, WHITE);
}
void maps::draw()
{
	Rectangle ABC;
	for (int i = 0; i < colums; i++)
	{
		for (int w = 0; w < rows; w++)
		{
			ABC = { (i * sizeGrid + _framework.x), (w * sizeGrid + _framework.y), (sizeGrid), (sizeGrid) };
			DrawRectangleLines(static_cast<int>(i * sizeGrid + _framework.x), static_cast<int>(w * sizeGrid + _framework.y), static_cast<int>(sizeGrid), static_cast<int>(sizeGrid), RED);
			if (_floor[i][w] == ground::tile)
			{
				DrawTexturePro(tx::floor, { 0,0,static_cast<float>(tx::floor.width),static_cast<float>(tx::floor.height) }, ABC, { 0,0 }, 0, WHITE);
			}
			if (_floor[i][w] == ground::deliverypoint)
			{
				DrawTexturePro(tx::dely, { 0,0,static_cast<float>(tx::floor.width),static_cast<float>(tx::floor.height) }, ABC, { 0,0 }, 0, WHITE);
			}
			if (_fisic[i][w] == objFisics::box)
			{
				DrawTexturePro(tx::box, { 0,0,static_cast<float>(tx::floor.width),static_cast<float>(tx::floor.height) }, ABC, { 0,0 }, 0, WHITE);
			}
			if (_fisic[i][w] == objFisics::pj)
			{
				DrawTexturePro(tx::pj, { 0,0,static_cast<float>(tx::pj.width),static_cast<float>(tx::pj.height) }, ABC, { 0,0 }, 0, WHITE);
			}
			showInfo();
#ifdef _DEBUG
			/*DrawRectangleLines(static_cast<int>(i * sizeGrid + _framework.x), static_cast<int>(w * sizeGrid + _framework.y), static_cast<int>(sizeGrid), static_cast<int>(sizeGrid), RED);
			if (_floor[i][w]== ground::tile)
			{
				DrawRectangle(static_cast<int>(i*sizeGrid+ _framework.x), static_cast<int>(w*sizeGrid+ _framework.y), static_cast<int>(sizeGrid), static_cast<int>(sizeGrid),WHITE);
			}
			if (_floor[i][w] == ground::deliverypoint)
			{
				DrawCircle(static_cast<int>(i * sizeGrid + _framework.x+ sizeGrid/2), static_cast<int>(w * sizeGrid + _framework.y+ sizeGrid/2), static_cast<float>(sizeGrid/2), GREEN);
			}
			if (_fisic[i][w] == objFisics::box)
			{
				DrawRectangle(static_cast<int>(i * sizeGrid + _framework.x), static_cast<int>(w * sizeGrid + _framework.y), static_cast<int>(sizeGrid/2), static_cast<int>(sizeGrid/2), BROWN);
			}
			if (_fisic[i][w] == objFisics::pj)
			{
				DrawRectangle(static_cast<int>(i * sizeGrid + _framework.x), static_cast<int>(w * sizeGrid + _framework.y), static_cast<int>(sizeGrid / 2), static_cast<int>(sizeGrid / 2), YELLOW);
			}
			showInfo();*/


#endif // _DEBUG
		}
	}
	DrawRectangleLinesEx(_framework, 1, WHITE);
}

void maps::clearFloorFisics()
{
	for (int i = 0; i < colums; i++)
	{
		for (int w = 0; w < rows; w++)
		{
			_floor[i][w] = ground::NONE;
			_fisic[i][w] = objFisics::NONE;
		}
	}
}

void maps::defaultClear()
{
	clearFloorFisics();
	rows = 5;
	colums = 5;
	sizeGrid = static_cast<float>(colums / rows);
	
}

void maps::putThinksInMap()
{
	for (int i = 0; i < colums; i++)
	{
		for (int w = 0; w < rows; w++)
		{
			if (CheckCollisionPointRec(GetMousePosition(), { _framework.x+i*sizeGrid,_framework.y+w*sizeGrid,sizeGrid,sizeGrid }))
			{
				if (IsKeyPressed(KEY_ONE))
				{
					_floor[i][w] = ground::NONE;
					_fisic[i][w] = objFisics::NONE;
				}
				if (IsKeyPressed(KEY_TWO))
				{
					_floor[i][w] = ground::tile;
				}
				if (IsKeyPressed(KEY_THREE))
				{
					_floor[i][w] = ground::deliverypoint;
				}
				if (_floor[i][w]==ground::tile|| _floor[i][w] == ground::deliverypoint)
				{
					if (IsKeyPressed(KEY_FOUR))
					{
						if (_fisic[i][w] != objFisics::NONE)
							_fisic[i][w] = objFisics::NONE;
						else
						_fisic[i][w] = objFisics::box;
					}
					if (IsKeyPressed(KEY_FIVE))
					{
						if (_fisic[i][w] != objFisics::NONE)
							_fisic[i][w] = objFisics::NONE;
						else
							_fisic[i][w] = objFisics::pj;
					}
				}
			}
		}
	}
}

int & maps::getRows()
{
	return rows;
}

int& maps::getColums()
{
	return colums;
}

maps::~maps()
{
	UnloadTexture(tx::box);
	UnloadTexture(tx::pj);
	UnloadTexture(tx::dely);
	UnloadTexture(tx::floor);
	UnloadTexture(control::tx::matrix);
}
