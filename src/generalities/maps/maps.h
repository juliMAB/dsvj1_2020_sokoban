#ifndef MAPS
#define MAPS
#include "generalities/generalities.h"
#include "scenes/gameplay/objs/generalitiesObjs.h"
enum class objFisics
{
	NONE,
	box,
	pj
};
enum class ground
{
	NONE,
	tile,
	deliverypoint
}; 
class maps
{
public:
	maps();
	maps(bool game, const std::string file);
	void setboxesRec();
	void setpjRec();
	void loadTexures();
	void defaultChargeData();
	void loadColumsrowsBoxes(char* level);
	void loadPosMono(char* data);
	void loadPosBoxes(char* data);
	void loadPosDelivery(char* data);
	void loadGround(char* data);
	void saveColumsrowsBoxes(char* level);
	void savePlayer(char* level);
	void saveDeliveryPoints(char* level);
	void saveBoxes(char* level);
	void SaveTiles(char* level);
	void saveMap(const std::string file);
	void chargeMap();
	void chargeMap(const std::string file);
	void createUpdateFramework();
	void createUpdateFrameworkGame();
	void updateTexture();
	void updateSizeGrid();
	void showInfo();
	void update();
	void updateCuanitys();
	void updateOnGame();
	void inputHacks();
	enableMove pjEnableMoves(Vector2 obj);
	void updatePlayerBox();
	void updateMatrix();
	void updateBox(Rectangle& obj, moving action, Vector2& pos);
	void animplayer();
	void updatePlayer(enableMove moves);
	void movePj(enableMove moves);
	bool win();
	bool lookArrown(int x, int y);
	void reset(std::string file);
	void drawGame();
	void draw();
	void clearFloorFisics();
	void defaultClear();
	void putThinksInMap();
	int & getRows();
	int & getColums();
	~maps();
private:
	ground _floor[control::Logic::maxGrill][control::Logic::maxGrill];
	objFisics _fisic[control::Logic::maxGrill][control::Logic::maxGrill];
	int rows;
	int colums;
	Rectangle _framework;
	Vector2 posMono;
	Vector2 posCajas[cuantitis::maxBoxes];
	Vector2 posDeliv[cuantitis::maxBoxes];
	float sizeGrid;
	int _cuantitiBoxes;
	moving pjAction;
	Rectangle pjRec;
	Rectangle pjInside;
	Rectangle boxesRec[cuantitis::maxBoxes];
	int pjTimer;
	float Merror; //margen de error de movimiento mientras la matriz sea mas peque�a.
};
#endif // !MAPS