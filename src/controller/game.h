#ifndef GAME_H
#define GAME_H

#include "raylib.h"
#include "generalities/generalities.h"
#include "scenes/menu/menu.h"
#include "scenes/levles/levels.h"
#include "scenes/gameplay/creator/creator.h"
#include "scenes/gameplay/gameplay.h"
#include "scenes/help/help.h"
#include "scenes/credits/credits.h"
#include <list>
namespace sokoban
{
	class game
	{
	public:
		game();
		~game();
		void run();
		void init();
		void resizeTextures();
		void changeScene();
		void finishCurrentScene();
		void initNewScene();
		void update();
		void updateScene();
		void updateSounds();
		void updateSoundsChange();
		void drawScene();
		void draw();
		void deinit();
	private:
		menu* menu1;
		lvls* levels1;
		creator* creator1;
		gameplay* gameplay1;
		credits* credits1;
		help* options1;
		button* _fx;
		button* _music;
	};
}


#endif