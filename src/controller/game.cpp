#include "game.h"


using namespace sokoban;
//---------------------
game::game()
{
	init();
}
game::~game()
{
	deinit();
}
//---------------------

void game::run()
{
	using namespace control;
	while (!WindowShouldClose() && !Logic::gameOver)
	{
		update();
		draw();
	}
	if (Logic::gameOver)  WindowShouldClose();

}
void game::init()
{
	using namespace control;
	using namespace Logic;
	//-------------------------RAYLIBSTUFF----------------------------
	control::screen::height = 600;
	control::screen::width = 800;
	InitWindow(control::screen::width, control::screen::height, "BOXxBOX");
	Image incon = LoadImage("../res/assets/pj1.png");
	SetTargetFPS(60);
	InitAudioDevice();
	SetExitKey(0);
	SetWindowIcon(incon);
	//---------------------------------------------------------------
	//---------------------------LOGIC-------------------------------
	gameOver = false;
	currentScene = control::Scene::Menu;
	newCurrentScene = currentScene;
	control::Logic::levelSelecct = 1;

	//--------------------------SOUNDS-------------------------------
	sounds::musicGeneral = LoadMusicStream("res/assets/menu.mp3");
	sounds::volumeGeneral = 1.0f;
	sounds::volumeFx = 1.0f;
	sounds::volumeMusic = 0.005f;
	SetMasterVolume(sounds::volumeGeneral/4);
	//---------------------------------------------------------------
	//---------------------OTHERS------------------------------------
	letters::small = GetScreenHeight() / 100;
	letters::medium = GetScreenHeight() / 50;
	letters::big = GetScreenHeight() / 25;
	//---------------------------------------------------------------
	//-----------------------FirstScreen-----------------------------
	menu1 = new menu();
	control::tx::fondo = LoadTexture("res/assets/fondo.png");
	buttons::sounds::tx = LoadTexture("res/assets/fxB.png");
	buttons::sounds::tx1 = LoadTexture("res/assets/musicB.png");
	buttons::menu::chargeButonAssets(LoadTexture("res/assets/botonVacio.png"), LoadSound("res/assets/botonS.wav"));
	buttons::sounds::tx.height = buttons::sounds::tx.height * GetScreenHeight() / 300;
	buttons::sounds::tx.width = buttons::sounds::tx.width * GetScreenWidth() / 400;
	buttons::sounds::tx1.height = buttons::sounds::tx1.height * GetScreenHeight() / 300;
	buttons::sounds::tx1.width = buttons::sounds::tx1.width * GetScreenWidth() / 400;
	control::tx::fondo.width = GetScreenWidth();
	control::tx::fondo.height = GetScreenHeight();
	_fx = new button({ static_cast<float>(screen::width - buttons::sounds::tx.width) , static_cast<float>(buttons::sounds::tx.height) }, buttons::sounds::tx );
	_music = new button({ static_cast<float>(screen::width - buttons::sounds::tx.width*2) ,static_cast<float>(buttons::sounds::tx1.height) }, buttons::sounds::tx1);
	//---------------------------------------------------------------
}
void game::resizeTextures()
{
	control::tx::fondo = LoadTexture("res/assets/fondo.png");
	buttons::sounds::tx = LoadTexture("res/assets/fxB.png");
	buttons::sounds::tx1 = LoadTexture("res/assets/musicB.png");
	buttons::menu::chargeButonAssets(LoadTexture("res/assets/botonVacio.png"), LoadSound("../res/assets/botonS.wav"));
	buttons::sounds::tx.height = buttons::sounds::tx.height * GetScreenHeight() / 300;
	buttons::sounds::tx.width = buttons::sounds::tx.width * GetScreenWidth() / 400;
	buttons::sounds::tx1.height = buttons::sounds::tx1.height * GetScreenHeight() / 300;
	buttons::sounds::tx1.width = buttons::sounds::tx1.width * GetScreenWidth() / 400;
	control::tx::fondo.width = GetScreenWidth();
	control::tx::fondo.height = GetScreenHeight();
	_fx = new button({ static_cast<float>(control::screen::width - buttons::sounds::tx.width) , static_cast<float>(buttons::sounds::tx.height) }, buttons::sounds::tx);
	_music = new button({ static_cast<float>(control::screen::width - buttons::sounds::tx.width * 2) ,static_cast<float>(buttons::sounds::tx1.height) }, buttons::sounds::tx1);

}
//---------------------
void game::changeScene()
{
	if (control::screen::changeResolution)
	{
		finishCurrentScene();
		control::screen::changeResolution = false;
		SetWindowSize(control::screen::currentresolution[control::screen::resolutionSelected][1], control::screen::currentresolution[control::screen::resolutionSelected][0]);
		initNewScene();
		resizeTextures();
	}
	if (control::Logic::newCurrentScene != control::Logic::currentScene) //me movi de escena, por lo tanto tengo de destruir la anterior y cargar la nueva.
	{
		if (control::Logic::newCurrentScene == control::Scene::Game && control::Logic::currentScene == control::Scene::Levels)
		{
			UnloadMusicStream(control::sounds::musicGeneral);
			control::sounds::musicGeneral = LoadMusicStream("res/assets/Game.mp3");
		}
		if (control::Logic::newCurrentScene == control::Scene::Levels && control::Logic::currentScene == control::Scene::Game)
		{
			UnloadMusicStream(control::sounds::musicGeneral);
			control::sounds::musicGeneral = LoadMusicStream("res/assets/menu.mp3");
		}
		finishCurrentScene();
		control::Logic::currentScene = control::Logic::newCurrentScene;
		initNewScene();
	}
	if (control::Logic::gameOver == true)
	{
		finishCurrentScene();
		control::Logic::currentScene = control::Scene::NONE;
	}
}
void game::finishCurrentScene()
{
	using namespace control;
	using namespace Logic;
	switch (currentScene)
	{
	case Scene::Creator:
		delete creator1;
		break;
	case Scene::Menu:
		delete menu1;
		break;
	case Scene::Game:
		delete gameplay1;
		break;
	case Scene::Help:
		delete options1;
		break;
	case Scene::Credits:
		delete credits1;
		break;
	case Scene::Levels:
		delete levels1;
		break;
	default:
		break;
	}
}
void game::initNewScene()
{
	using namespace control;
	using namespace Logic;
	switch (control::Logic::currentScene)
	{
	case Scene::Creator:
		creator1 = new creator();
		break;
	case Scene::Menu:
		menu1 = new menu();
		break;
	case Scene::Game:
		gameplay1 = new gameplay();
		break;
	case Scene::Help:
		options1 = new help();
		break;
	case Scene::Credits:
		credits1 = new credits();
		break;
	case Scene::Levels:
		levels1 = new lvls();
		break;
	default:
		break;
	}
}
//---------------------
void game::update()
{
	using namespace control;
	//----------------KEY TO EXIT---------------------
	if (IsKeyPressed(KEY_ESCAPE))
	{
		Logic::gameOver = true;
	}
	//------------------------------------------------
	if (_fx->actualizar()) {
		if (control::sounds::volumeFx != 0)
			control::sounds::volumeFx = 0;
		else
			control::sounds::volumeFx = 1.0f;
		updateSoundsChange();
	}
	if (_music->actualizar()) {
		if (control::sounds::volumeMusic != 0)
			control::sounds::volumeMusic = 0;
		else
			control::sounds::volumeMusic = 1.0f;
		updateSoundsChange();
	}
	updateSounds();
	updateScene();
	changeScene();
}
void game::updateScene()
{
	using namespace control;
	switch (control::Logic::currentScene)
	{
	case Scene::Creator:
		creator1->update();
		break;
	case Scene::Menu:
		menu1->update();
		break;
	case Scene::Game:
		gameplay1->update();
		break;
	case Scene::Help:
		options1->update();
		break;
	case Scene::Credits:
		credits1->update();
		break;
	case Scene::Levels:
		levels1->update();
		break;
	default:
		break;
	}
}
void game::updateSounds()
{
	
	//add sounds...

	if (IsMusicPlaying(control::sounds::musicGeneral))
	{
		UpdateMusicStream(control::sounds::musicGeneral);
	}
	else
	{
		
		PlayMusicStream(control::sounds::musicGeneral);
		
	}
}
void game::updateSoundsChange()
{
	SetSoundVolume(buttons::menu::sd, control::sounds::volumeFx);
	SetMusicVolume(control::sounds::musicGeneral, control::sounds::volumeMusic);
}
//---------------------
void game::drawScene()
{
	using namespace control;
	using namespace Logic;
	
	if (!gameOver)
	{
		switch (currentScene)
		{
		case Scene::Creator:
			creator1->draw();
			break;
		case Scene::Menu:
			menu1->draw();
			break;
		case Scene::Game:
			gameplay1->draw();
			break;
		case Scene::Help:
			options1->draw();
			break;
		case Scene::Credits:
			credits1->draw();
			break;
		case Scene::Levels:
			levels1->draw();
			break;
		default:
			break;
		}
	}
}
void game::draw()
{
	BeginDrawing();
	ClearBackground(BLACK);
	DrawTexture(control::tx::fondo, 0, 0, WHITE);
	drawScene();
	_fx->drawButon(buttons::sounds::tx);
	_music->drawButon(buttons::sounds::tx1);
	EndDrawing();
}
//---------------------
void game::deinit()
{
	UnloadMusicStream(control::sounds::musicGeneral);
	//UnloadMusicStream
	buttons::menu::dischargButonAssets();
	CloseWindow();
	finishCurrentScene();
}
