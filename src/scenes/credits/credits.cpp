#include "credits.h"

credits::credits()
{
	_texture = LoadTexture("res/assets/credits.png");
	back = new button({ 0,static_cast<float>(GetScreenHeight() / 10 * 8) }, buttons::menu::tx, true, "BACK");
}

credits::~credits()
{
	delete back;
	UnloadTexture(_texture);
}

void credits::update()
{
	if (back->actualizar())
		control::Logic::newCurrentScene = control::Scene::Menu;
}

void credits::draw()
{
	DrawTexture(_texture, 0, 0, WHITE);
	back->drawButon(buttons::menu::tx);
#ifdef _DEBUG
	DrawText("press F1 to pass to next lvl", GetScreenWidth() / 2, GetScreenHeight() / 2, control::letters::big, WHITE);
#endif // _DEBUG

	
}
