#ifndef CREDITS
#define CREDITS
#include "raylib.h"
#include "generalities/button/button.h"
#include "generalities/generalities.h"
#include <iostream>
class credits
{
public:
	credits();
	~credits();
	void update();
	void draw();

private:
	Texture2D _texture;
	button* back;
};
#endif // !MENU
