#include "help.h"

help::help()
{
	_texture = LoadTexture("res/assets/options.png");
	back = new button({ 0,static_cast<float>(GetScreenHeight() / 10 * 8) }, buttons::menu::tx, true,"BACK");
	fx = new buttonUpDown({ static_cast<float>(GetScreenWidth()/10*2),static_cast<float>(GetScreenHeight()/10*2),static_cast<float>(GetScreenWidth() / 10 * 2),static_cast<float>(GetScreenHeight() / 10 * 2) }, buttons::menu::tx);
	music = new buttonUpDown({ static_cast<float>(GetScreenWidth() / 10 * 7),static_cast<float>(GetScreenHeight() / 10 * 2),static_cast<float>(GetScreenWidth() / 10 * 2),static_cast<float>(GetScreenHeight() / 10 * 2) }, buttons::menu::tx);
	resolution = new buttonUpDown({ static_cast<float>(GetScreenWidth() / 10 * 2),static_cast<float>(GetScreenHeight() / 10 * 7),static_cast<float>(GetScreenWidth() / 10 * 2),static_cast<float>(GetScreenHeight() / 10 * 2) }, buttons::menu::tx);
	fullscreen = new button({ static_cast<float>(GetScreenWidth() / 10 * 7),static_cast<float>(GetScreenHeight() / 10 * 7) }, buttons::menu::tx, true, "FULLSCREEN");
}

help::~help()
{
	delete back;
	delete fx;
	delete music;
	delete resolution;
	UnloadTexture(_texture);
}

void help::update()
{
	if (back->actualizar())
		control::Logic::newCurrentScene = control::Scene::Menu;
	if(fx->actualizarF(control::sounds::volumeFx,0,1))
		SetSoundVolume(buttons::menu::sd, control::sounds::volumeFx);
	if(music->actualizarF(control::sounds::volumeMusic, 0, 1))
		SetMusicVolume(control::sounds::musicGeneral, control::sounds::volumeMusic);
	if (resolution->actualizar(control::screen::resolutionSelected, 0, 3))
	{
		control::screen::width = control::screen::currentresolution[control::screen::resolutionSelected][0];
		control::screen::height = control::screen::currentresolution[control::screen::resolutionSelected][1];
		control::screen::changeResolution = true;
	}
	if (fullscreen->actualizar(buttons::menu::sd))
		ToggleFullscreen();
}

void help::draw()
{
	DrawTexture(_texture,0,0,WHITE);
	back->drawButon(buttons::menu::tx);
	fx->drawButonF(buttons::menu::tx, control::sounds::volumeFx, "FX: ");
	music->drawButonF(buttons::menu::tx, control::sounds::volumeMusic, "MUSIC: ");
	resolution->drawButon(buttons::menu::tx, control::screen::resolutionSelected , "RESOLUTION");
	fullscreen->drawButon(buttons::menu::tx);
}
