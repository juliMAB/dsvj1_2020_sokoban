#ifndef HELP
#define HELP
#include "raylib.h"
#include "generalities/button/button.h"
#include "generalities/generalities.h"
#include <iostream>
class help
{
public:
	help();
	~help();
	void update();
	void draw();

private:
	Texture2D _texture;
	button* back;
	buttonUpDown* fx;
	buttonUpDown* music;
	buttonUpDown* resolution;
	button* fullscreen;
};
#endif // !MENU
