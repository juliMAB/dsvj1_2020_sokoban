#include "nextLvl.h"

nextLevel::nextLevel()
{
	control::tx::star = LoadTexture("res/assets/star.png");
	activo = false;
	tx = LoadTexture("res/assets/marco.png");
	_rec = { static_cast<float>(control::screen::width / 10 * 2),static_cast<float>(control::screen::height / 10 * 2),static_cast<float>(control::screen::width / 10 * 6),static_cast<float>(control::screen::height / 10 * 6) };
	tx.width = static_cast<int>(_rec.width);
	tx.height = static_cast<int>(_rec.height);
	control::tx::star.width = tx.width / 10;
	control::tx::star.height = tx.height / 10;
	back = new button({ _rec.x, _rec.y + _rec.height / 4 * 4 }, "BACK", buttons::menu::tx);
	next = new button({ _rec.x + _rec.width, _rec.y + _rec.height / 4 * 4 }, "NEXT", buttons::menu::tx);
	reset = new button({ _rec.x+ _rec.width/2, _rec.y + _rec.height / 4 * 2 }, "RESET", buttons::menu::tx);
	timer = 0;
}

void nextLevel::offno()
{
	activo = !activo;
}





bool nextLevel::resetB()
{
	if (activo)
	{
		if (reset->actualizar(buttons::menu::sd))
		{
			return true;
		}
		return false;
	}
	return false;
}

void nextLevel::saveStars(int time)
{
	control::leveles::stars[control::Logic::currentLevel] = 1;
	if (time<150)
	{
		control::leveles::stars[control::Logic::currentLevel] = 2;
		if (time < 50)
		{
			control::leveles::stars[control::Logic::currentLevel] = 3;
		}
	}
	timer = time;
	
}



void nextLevel::backB()
{
	if (activo)
	{
		if (back->actualizar(buttons::menu::sd))
			control::Logic::newCurrentScene = control::Scene::Levels;
	}
}

bool nextLevel::nextL()
{
	if (activo)
	{
		if (next->actualizar(buttons::menu::sd))
		{
			return true;
		}
		return false;
	}
	return false;
}

void nextLevel::draw()
{
	if (activo)
	{
		DrawTexturePro(tx, { 0,0,static_cast<float>(tx.width),static_cast<float>(tx.height) }, _rec, { 0,0 }, 0, WHITE);
		for (int i = 0; i < control::leveles::stars[control::Logic::currentLevel]; i++)
		{
			DrawTexturePro(control::tx::star, { 0,0,static_cast<float>(control::tx::star.width),static_cast<float>(control::tx::star.height) }, { static_cast<float>(_rec.x + _rec.width / 5 + i * control::tx::star.width),_rec.y,static_cast<float>(control::tx::star.width),static_cast<float>(control::tx::star.height) }, { 0,0 }, 0, WHITE);
		}
		DrawText(FormatText("TIME USED: %i", timer), static_cast<int>(_rec.x + _rec.width / 2), static_cast<int>(_rec.y + _rec.height / 10), control::letters::big, RED);
		back->drawButon(buttons::menu::tx);
		next->drawButon(buttons::menu::tx);
		reset->drawButon(buttons::menu::tx);
	}
}

nextLevel::~nextLevel()
{
	delete back;
	delete next;
	delete reset;
	UnloadTexture(tx);
}
