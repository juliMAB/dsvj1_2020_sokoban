#ifndef PAUSE
#define PAUSE
#include "generalities/button/button.h"
class pause
{
public:
	pause();
	void offno();
	void update();
	void draw();
	bool getActivo();
	~pause();

private:
	button* back;
	button* resume;
	Rectangle _rec;
	Texture2D tx;
	bool activo;
};

#endif // !PAUSE
