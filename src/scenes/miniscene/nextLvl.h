#ifndef NEXTLEVEL
#define NEXTLEVEL
#include "generalities/button/button.h"
class nextLevel
{
public:
	nextLevel();
	void offno();
	bool resetB();
	void saveStars(int time);
	bool nextL();
	void backB();
	void draw();
	~nextLevel();

private:
	button* back;
	button* next;
	button* reset;
	Rectangle _rec;
	Texture2D tx;
	bool activo;
	int timer;
};

#endif // !PAUSE