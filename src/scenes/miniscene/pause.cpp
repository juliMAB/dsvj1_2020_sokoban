#include "pause.h"

pause::pause()
{
	activo = false;
	tx = LoadTexture("res/assets/marco.png");
	_rec = { static_cast<float>(control::screen::width / 10 * 2),static_cast<float>(control::screen::height / 10 * 2),static_cast<float>(control::screen::width / 10 * 6),static_cast<float>(control::screen::height / 10 * 6) };
	tx.width = static_cast<int>(_rec.width);
	tx.height = static_cast<int>(_rec.height);
	back = new button({ _rec.x + _rec.width / 2, _rec.y + _rec.height / 4*4 }, "BACK", buttons::menu::tx);
	resume = new button({ _rec.x + _rec.width / 2, _rec.y + _rec.height / 4 * 2 }, "RESUME", buttons::menu::tx);
}

void pause::offno()
{
	if (IsKeyPressed(KEY_P))
	{
		activo = !activo;
	}
}

void pause::update()
{
	if (activo)
	{
		if (back->actualizar(buttons::menu::sd))
			control::Logic::newCurrentScene = control::Scene::Levels;
		if (resume->actualizar(buttons::menu::sd))
			activo = false;
	}
}

void pause::draw()
{
	if (activo)
	{
		DrawTexturePro(tx, { 0,0,static_cast<float>(tx.width),static_cast<float>(tx.height) }, _rec, { 0,0 }, 0, WHITE);
		back->drawButon(buttons::menu::tx);
		resume->drawButon(buttons::menu::tx);
	}
}

bool pause::getActivo()
{
	return activo;
}

pause::~pause()
{
	delete back;
	delete resume;
	UnloadTexture(tx);
}
