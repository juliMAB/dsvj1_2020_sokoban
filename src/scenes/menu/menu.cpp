#include "menu.h"

menu::menu()
{
	//-----------------------------Gias--------------------------------
	float h = static_cast<float>(GetScreenHeight() / 12);
	float w = static_cast<float>(GetScreenWidth() / 2);
	//--------------------------------------------------------------
	//----------------Carga de Assets--------------------------------
	_texture = LoadTexture("res/assets/MenuStatic.png");
	
	_texture.height = _texture.height * GetScreenHeight() / 600;
	_texture.width = _texture.width * GetScreenWidth() / 800;
	int A = GetScreenHeight() / 15*7;
	buttons::menu::chargeButonAssets(LoadTexture("res/assets/botonVacio.png"), LoadSound("../res/assets/botonS.wav"));
	buttons::menu::tx.height = static_cast<int>(static_cast<float>(GetScreenHeight()) / 15.5f * 2.0f);
	buttons::menu::tx.width = static_cast<int>(static_cast<float>(GetScreenWidth()) / 4.8f * 1.0f);
	//--------------------------------------------------------------
	//----------------Crear Botones--------------------------------
	//--------------------------------------------------------------
	_buton[static_cast<int>(buttonsMenu::play)] = new button({ w,A + h * (0) }, "PLAY", buttons::menu::tx);
	_buton[static_cast<int>(buttonsMenu::help)] = new button({ w,A + h * (1) }, "HELP", buttons::menu::tx);
	_buton[static_cast<int>(buttonsMenu::credits)] = new button({ w,A + h * (2) }, "CREDITS", buttons::menu::tx);
	_buton[static_cast<int>(buttonsMenu::close)] = new button({ w,A + h * (3) }, "EXIT", buttons::menu::tx);
}

menu::~menu()
{
	//----------------delete Botones--------------------------------
	for (int i = 0; i < static_cast<int>(buttonsMenu::cuantity); i++)
	{
		delete _buton[i];
	}
	//--------------------------------------------------------------
	//----------------Unload textures--------------------------------
	UnloadTexture(_texture);
	
	//--------------------------------------------------------------
}

void menu::update()
{
	//----------------Update Botones--------------------------------
	using namespace control;
	using namespace Logic;
	if (_buton[static_cast<int>(buttonsMenu::play)]->actualizar(buttons::menu::sd))
		newCurrentScene = Scene::Levels;
	if (_buton[static_cast<int>(buttonsMenu::help)]->actualizar(buttons::menu::sd))
		newCurrentScene = Scene::Help;
	if (_buton[static_cast<int>(buttonsMenu::credits)]->actualizar(buttons::menu::sd))
		newCurrentScene = Scene::Credits;
	if (_buton[static_cast<int>(buttonsMenu::close)]->actualizar(buttons::menu::sd))
		gameOver = true;
	//--------------------------------------------------------------
#ifndef DEBUG
	if (IsKeyReleased(KEY_H))
	{
		std::cout << "----------updating menu----------" <<std::endl;
	}
#endif // !DEBUG
}

void menu::draw()
{
	//-----------Fondo----------------
	Rectangle A= { 0,0,static_cast<float>(_texture.width) ,static_cast<float>(_texture.height) };
	Vector2 B = { static_cast<float>(GetScreenWidth() / 2 - _texture.width / 2),static_cast<float>(GetScreenHeight() / 2 - _texture.height / 2) };
	DrawTextureRec(_texture, A, B,WHITE);
	//--------------------------------
	//-----------Botones--------------
	for (int i = 0; i < static_cast<int>(buttonsMenu::cuantity); i++)
	{
		_buton[i]->drawButon(buttons::menu::tx);
	}
	//--------------------------------
#ifndef DEBUG
	if (IsKeyReleased(KEY_H))
	{
		std::cout << " -  Drawing Menu  - " << std::endl;
	}
#endif // !DEBUG
}