#ifndef MENU
#define MENU
#include "raylib.h"
#include "generalities/button/button.h"
#include "generalities/generalities.h"
#include <iostream>
class menu
{
public:
	menu();
	~menu();
	void update();
	void draw();

private:
	Texture2D _texture; //se entiende de sobra que es el fondo.
	//Texture2D _textureButons;
	button* _buton[static_cast<int>(buttonsMenu::cuantity)];
};
#endif // !MENU
