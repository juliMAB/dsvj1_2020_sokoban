#include "levels.h"

lvls::lvls()
{
	//-----------------------------cargar txchiquita------------------------------------
	_txBshrt = LoadTexture("res/assets/botonVacioChiquito.png");
	_txBshrt.width = static_cast<int>(static_cast<float>(GetScreenWidth()) / 20);
	_txBshrt.height = static_cast<int>(static_cast<float>(GetScreenWidth()) / 20);
	//-----------------------------coloco fondo-----------------------------------------
	_txFondo = LoadTexture("res/assets/LevelStatic.png");
	_txFondo.width = static_cast<int>(static_cast<float>(GetScreenWidth()) / 2.0f);
	_txFondo.height = static_cast<int>(static_cast<float>(GetScreenWidth()) / 2.0f);
	//----------------------------cargar txLarga----------------------------------------
	_txBlarg = LoadTexture("res/assets/botonVacio.png");
	_txBlarg.width = static_cast<int>(static_cast<float>(GetScreenWidth()) / 12);
	_txBlarg.height = static_cast<int>(static_cast<float>(GetScreenWidth()) / 20);

	//------------------------------Gias--------------------------------------------------------
	control::tx::star = LoadTexture("res/assets/star.png");
	control::tx::star.height = _txBshrt.height/3;
	control::tx::star.width = _txBshrt.width/3;
	short separationB = 10;
	short separationA = GetScreenHeight()/20;
	float A = static_cast<float>(GetScreenWidth() / 2 - _txFondo.width/2);
	float B = static_cast<float>(GetScreenHeight() / 2 - _txFondo.height / 2);
	Vector2 colocator = { A+5+_txBshrt.width,B+5+_txBshrt.height };
	for (int i = 0; i < control::Logic::levelsMax; i++)
	{
		PosButons[i] = colocator;
		colocator.x += separationB + _txBshrt.width;
		if (colocator.x+_txBshrt.width > A+_txFondo.width)
		{
			colocator.y += separationA;
			colocator.x = A + 5 + _txBshrt.width;
		}
	}
	//-----------------------------colocar boton Lv------------------------------------------------
	for (int i = 0; i < control::Logic::levelsMax; i++)
	{
		_butons[i] = new button(PosButons[i], FormatText("%01i", i+1), _txBshrt);
	}
	//------------------------------Colocar botones ex--------------------------------------------
	A = static_cast<float>(static_cast<float>(GetScreenWidth()) / 2 - _txFondo.width / 2);
	B = static_cast<float>(static_cast<float>(GetScreenHeight()) / 2 - _txFondo.height / 2);
	float BordeX = static_cast<float>(_txFondo.width / 10);
	float BordeY = static_cast<float>(_txFondo.height / 50);
	colocator = { A+ _txBlarg.width/2 + BordeX,B + _txFondo.height-_txBlarg.height - BordeY };
	_backB = new button(colocator,"BACK", _txBlarg);
	colocator = { A+_txFondo.width-( _txBlarg.width / 2 + BordeX),B + _txFondo.height - _txBlarg.height - BordeY };
	_playB = new button(colocator, "PLAY", _txBlarg);
	colocator = { static_cast<float>(GetScreenWidth() / 2),B + _txFondo.height - _txBlarg.height - BordeY };
	_creatorB = new button(colocator, "CREATOR", _txBlarg);
	//---------------------------------------------------------------------------------------------
}
void lvls::update()
{
	//----------------------------botoncitos-------------------------------------
	for (int i = 0; i < control::Logic::levelsMax; i++)
	{
		if (i<= control::Logic::unlookLevel)
		{
			if (_butons[i]->actualizar(buttons::menu::sd))
				control::Logic::currentLevel = i;
		}
	}
	//-------------------------otros------------------------------------------
	if (_backB->actualizar(buttons::menu::sd))
		control::Logic::newCurrentScene = control::Scene::Menu;
	if (_playB->actualizar(buttons::menu::sd))
		control::Logic::newCurrentScene = control::Scene::Game;
	if (_creatorB->actualizar(buttons::menu::sd))
		control::Logic::newCurrentScene = control::Scene::Creator;
}
void lvls::draw()
{
	//-----------Fondo----------------
	Rectangle A = { 0,0,static_cast<float>(_txFondo.width) ,static_cast<float>(_txFondo.height) };
	Vector2 B = { static_cast<float>(GetScreenWidth() / 2 - _txFondo.width / 2),static_cast<float>(GetScreenHeight() / 2 - _txFondo.height / 2) };
	DrawTextureRec(_txFondo, A, B, WHITE);
	//--------------------------------

	for (int i = 0; i < control::Logic::levelsMax; i++)
	{
		_butons[i]->drawButon(_txBshrt);
		if (control::leveles::stars[i]>0&& control::leveles::stars[i] < 4)
		{
			for (int w = 0; w < control::leveles::stars[i]; w++)
			{
				DrawTexture(control::tx::star, static_cast<int>(PosButons[i].x-control::tx::star.width*w), static_cast<int>(PosButons[i].y), WHITE);
			}
		}
	}

	_backB->drawButon(_txBlarg);
	_playB->drawButon(_txBlarg);
	_creatorB->drawButon(_txBlarg);
}
lvls::~lvls()
{
	UnloadTexture(_txBlarg);
	UnloadTexture(_txBshrt);
	UnloadTexture(_txFondo);
	for (int i = 0; i < control::Logic::levelsMax; i++)
	{
		delete _butons[i];
	}
	delete _backB;
	delete _playB;
}
