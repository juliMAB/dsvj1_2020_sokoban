#ifndef LEVELS
#define LEVELS
#include "raylib.h"
#include "generalities/generalities.h"
#include "generalities/button/button.h"
#include <string>
class lvls
{
public:
	lvls();
	void update();
	void draw();
	~lvls();

private:
	Texture2D _txFondo;
	Texture2D _txBlarg;
	Texture2D _txBshrt;
	button* _butons[control::Logic::levelsMax];
	button* _backB;
	button* _creatorB;
	button* _playB;
	Vector2 PosButons[control::Logic::levelsMax];
};

#endif // !LVLS
