#ifndef GENERALITIESOBJS
#define GENERALITIESOBJS
#include "raylib.h"	
namespace cuantitis
{
	const int maxBoxes = 20;
	const int stm = 15; //steps to move
}
namespace tx
{
	extern Texture2D box;
	extern Texture2D pj;
	extern Texture2D floor;
	extern Texture2D dely;
	extern Texture2D wall;
}
enum class moving
{
	stop,
	right,
	left,
	up,
	down
};
struct enableMove
{
	bool up;
	bool down;
	bool left;
	bool right;
};
#endif // !GENERALITIESOBJS