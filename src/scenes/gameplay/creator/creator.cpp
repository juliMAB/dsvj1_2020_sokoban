#include "creator.h"

creator::creator()
{
	map = new maps();
	RowsS = new buttonUpDown({ 0,static_cast<float>(control::screen::height / 10 * 1),static_cast<float>(control::screen::width / 10 * 1),static_cast<float>(control::screen::height / 10) }, buttons::menu::tx);
	ColumsS = new buttonUpDown({ 0,static_cast<float>(control::screen::height / 10 * 3),static_cast<float>(control::screen::width / 10 * 1) ,static_cast<float>(control::screen::height / 10) }, buttons::menu::tx);
	levelS = new buttonUpDown({ 0,static_cast<float>(control::screen::height / 10 * 6),static_cast<float>(control::screen::width / 10 * 1) ,static_cast<float>(control::screen::height / 10) }, buttons::menu::tx);
	back = new button({ static_cast<float>(GetScreenWidth()  / 10 * 5),static_cast<float>(GetScreenHeight() / 10 * 9) }, buttons::menu::tx,true, "BACK");
}

void creator::selectSlot()
{
	int aux = control::Logic::levelSelecct;
	if(levelS->actualizar(control::Logic::levelSelecct,0,control::Logic::levelsMax))
	{
		saveMap(control::leveles::L[aux].c_str());
		chargeMap();
	}
	
}

void creator::controlRowsColums()
{
	RowsS->actualizar(map->getRows(),5,control::Logic::maxGrill);
	ColumsS->actualizar(map->getColums(),5, control::Logic::maxGrill);
	map->createUpdateFramework();
}

void creator::clear()
{
	map->defaultClear();
}

void creator::update()
{
	selectSlot();
	controlRowsColums();
	setObject();
	map->update();
	if (back->actualizar(buttons::menu::sd))
	{
		saveMap(LoadFileText(control::leveles::L[control::Logic::levelSelecct].c_str()));
		control::Logic::newCurrentScene = control::Scene::Levels;
	}
		
}

void creator::setObject()
{
	map->putThinksInMap();
}

void creator::inputs()
{
	if (IsKeyPressed(KEY_C))
		clear();
	if (IsKeyPressed(KEY_R))
		map->reset(control::leveles::L[control::Logic::levelSelecct].c_str());
	if (IsKeyPressed(KEY_S))
		saveMap(LoadFileText(control::leveles::L[control::Logic::levelSelecct].c_str()));
}

void creator::chargeMap()
{
	map->chargeMap();
}

void creator::saveMap(const std::string file)
{
	map->saveMap(file);
}

void creator::draw()
{
	map->draw();
	RowsS->drawButon(buttons::menu::tx,map->getRows(),"rows");
	ColumsS->drawButon(buttons::menu::tx,map->getColums(),"colums");
	levelS->drawButon(buttons::menu::tx,control::Logic::levelSelecct,"level");
	back->drawButon(buttons::menu::tx);
	DrawText("Use 1 to put void, 2 to flor,3 devP,4 box,5 pj",control::screen::width/2-MeasureText("Use 1 to put void, 2 to flor,3 Box,4 devP,5 pj",control::letters::medium),GetScreenHeight()/10*8,control::letters::medium,WHITE);
}

creator::~creator()
{
	delete map;
	delete RowsS;
	delete ColumsS;
	delete back;
	delete levelS;
}
