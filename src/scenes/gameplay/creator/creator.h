#ifndef CREATOR
#define CREATIR
#include "generalities/maps/maps.h"
#include "generalities/button/button.h"
class creator
{
public:
	creator();
	void selectSlot();
	void controlRowsColums();
	void clear();
	void update();
	void setObject();
	void inputs();
	void chargeMap();
	void saveMap(const std::string file);
	void draw();
	~creator();

private:
	maps* map;
	buttonUpDown* RowsS;
	buttonUpDown* ColumsS;
	buttonUpDown* levelS;
	button* back;
};
#endif // !CREATOR
