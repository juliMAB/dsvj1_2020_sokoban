#include "gameplay.h"

gameplay::gameplay()
{
	inittime = 0;
	currenttime = inittime;
	map = new maps(true, control::leveles::L[control::Logic::currentLevel]);
	pause1 = new pause();
	next = new nextLevel();
	gameOver = false;
}

void gameplay::loadmap()
{
	map->chargeMap(control::leveles::L[control::Logic::currentLevel]);
}

void gameplay::update()
{
	if (!gameOver)
	{
		if (pause1->getActivo())
		{
			pause1->offno();
			pause1->update();
		}
		else
		{
			map->updateOnGame();
			pause1->offno();
			currenttime += GetFrameTime();
		}
		if (map->win())
		{
			control::Logic::unlookLevel = control::Logic::currentLevel + 1;
			gameOver = true;
			next->offno();
			next->saveStars(static_cast<int>(currenttime));
			currenttime = inittime;
		}
#ifndef DEBUG
		if (IsKeyPressed(KEY_F1))
		{
			gameOver = true;
			next->offno();
			next->saveStars(static_cast<int>(currenttime));
		}
#endif // !_DEBUG

	}
	else
	{
		if (next->resetB())
		{
			currenttime = inittime;
			map->reset(control::leveles::L[control::Logic::currentLevel]);
			gameOver = false;
			next->offno();
		}
		if (next->nextL())
		{
			control::Logic::currentLevel++;
			map->reset(control::leveles::L[control::Logic::currentLevel]);
			gameOver = false;
			next->offno();
		}
		next->backB();
	}
	
	
}

void gameplay::draw()
{
	map->drawGame();
	pause1->draw();
	next->draw();
	DrawText(FormatText("TIMER:%i",static_cast<int>(currenttime)),GetScreenWidth()/2,0,control::letters::big,GREEN);
}

gameplay::~gameplay()
{
	delete map;
	delete pause1;
	delete next;
}
