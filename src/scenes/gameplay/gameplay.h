#ifndef GAMEPLAY
#define GAMEPLAY
#include "raylib.h"
#include "scenes/miniscene/pause.h"
#include "scenes/miniscene/nextLvl.h"
#include "generalities/maps/maps.h"
class gameplay
{
public:
	gameplay();
	void loadmap();
	void update();
	void draw();
	~gameplay();

private:
	maps* map;
	pause* pause1;
	nextLevel* next;
	float inittime;
	float currenttime;
	bool gameOver;
};


#endif // !GAMEPLAY
